var serverHelper = require('./../server/utility/server-helper');
var customCatchError = serverHelper.customCatchError;
const express = serverHelper.express;
const router = express.Router();
const Category = require('./../modules/category');
const category = new Category();

router.get("/getCategories", async(req, res) => {
	try {
		category.getCategories(req.params, (error, result) => {
			res.status(200).send(result);
		})
	} catch(error) {
		console.log(error)
		res.status(500).json({message: customCatchError()})
	}
})

router.post("/getCategory", async(req, res) => {
	try {
		category.getCategory(req.body, (error, result) => {
			res.status(200).send(result);
		})
	} catch(error) {
		console.log(error)
		res.status(500).json({message: customCatchError()})
	}
})

router.post("/createCategory", async(req, res) => {
	try {
		category.createCategory(req.body, (error, result) => {
			res.status(200).send(result);
		})
	} catch(error) {
		console.log(error)
		res.status(500).json({message: customCatchError()})
	}
})

router.post("/updateCategory", async(req, res) => {
	try {
		category.updateCategory(req.body, (error, result) => {
			res.status(200).send(result);
		})
	} catch(error) {
		console.log(error)
		res.status(500).json({message: customCatchError()})
	}
})


module.exports = router;