var serverHelper = require('./../server/utility/server-helper');
var customCatchError = serverHelper.customCatchError;
const express = serverHelper.express;
const router = express.Router();
const Product = require('./../modules/product');
const product = new Product();

router.get('/getProducts', async (req, res) => {
  try {
    product.getProducts(req.query, (error, result) => {
      // console.log("functionCall error==>", error);
      // console.log("functionCall result==>", result);
      res.status(200).send(result);
    })
  } catch (err) {
    console.log("Error occured =>", err.message)
    res.status(500).json({ message: customCatchError() });
  }
});

router.post('/getProduct', async (req, res) => {
  try {
    product.getProduct(req.body, (error, result) => {
      // console.log("functionCall error==>", error);
      // console.log("functionCall result==>", result);
      res.status(200).send(result);
    })
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

router.post('/createProduct', async (req, res) => {
  try {
    product.createProduct(req.body, (error, result) => {
      // console.log("functionCall error==>", error);
      // console.log("functionCall result==>", result);
      res.status(200).send(result);
    })
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

router.post('/updateProduct', async (req, res) => {
  try {
    product.updateProduct(req.body, (error, result) => {
      // console.log("functionCall error==>", error);
      // console.log("functionCall result==>", result);
      res.status(200).send(result);
    })
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

module.exports = router;
