'use strict';
var serverHelper = require('./utility/server-helper.js');
var knex = serverHelper.knex;
var datasources = serverHelper.datasources;
var _ = serverHelper._;


var requiredKeys = ['user', 'password', 'host', 'database', 'name', 'connector'];
var connect = () => {
	var dbConnectObj = {};
	_.forEach(datasources, (eachSource, eachKey) => {
		_.forEach(requiredKeys, (KeyValue) => {
			// console.log('_.has(eachSource[KeyValue])', KeyValue, _.has(eachSource, KeyValue), eachSource[KeyValue])
			if (_.has(eachSource, KeyValue)) {

			} else {
				throw new Error('Invalid database configuration');
			}
		});
		var conn = knex({
			client: eachSource.connector,
			connection: {
				host: eachSource.host,
				user: eachSource.user,
				password: eachSource.password,
				database: eachSource.database,
			}
		});
		dbConnectObj[eachSource.name] = conn;
	});
	// console.log('dbConnectObj', dbConnectObj);
	return dbConnectObj;
};

// connect();
module.exports = connect;