'use strict';
var serverHelper = require('./utility/server-helper.js');
var express = serverHelper.express;
var config = serverHelper.config;
var bodyParser = serverHelper.bodyParser;
var port = process.env.PORT || config.port;
const productRoutes = require('./../routes/product-remote');
const categoryRoutes = require('./../routes/category-remote');

global.app = express();
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.json());
app.use(config.restApiRoot+'product', productRoutes);
app.use(config.restApiRoot+'category', categoryRoutes);

app.start = function() {
	console.log("herer")
	// start the web server
	var server = app.listen(port, function() {
		var port = server.address().port;
		console.log("MiniNode listening at port ", port);
		return server;
	});
};

app.start()

module.exports = app;