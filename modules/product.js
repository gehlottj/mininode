'use strict';
var serverHelper = require('./../server/utility/server-helper');
var dbConn = require('./../server/db-connect')()["mariadb"];
var async = serverHelper.async;
var customCatchError = serverHelper.customCatchError;
var _ = serverHelper._;

class Product {
	constructor() {
		this.tableName = "product"
	}

	getProducts(data, cb) {
		console.log("in here", data)
		async.auto({
	        validate: (callback) => {
	            var validate = true;

	            if (!validate) {
	                return callback("Please provide correct data");
	            }
	            // data = validatorFunctions.trimAll(data);
	            return callback(null, data);
	        },
	        getProducts : ['validate', (results, callback) => {
	        	dbConn.raw("select * from product order by created_at DESC").then((result) => {
					// console.log(result);
					result = JSON.parse(JSON.stringify(result[0]))
					return callback(null, result);
				}).catch((error) => {
					console.log("Error occured while getting data", error)
					return callback({msg:"Something went wrong, we are working on it!"});
				});
	        }]
	    }, (error,asyncAutoResult) => {
	    	if(error) {
	    		console.log(error);
	    		return cb(null, customCatchError(error));
	    	} else {
				return cb(null, {success: true, data:asyncAutoResult.getProducts});
	    	}
	    });
	}

	getProduct(data, cb) {
		console.log("in here", data)
		async.auto({
	        validate: (callback) => {
	            var validate = true;

	            if (!validate) {
	                return callback("Please provide correct data");
	            }
	            return callback(null, data);
	        },
	        getProduct : ['validate', (results, callback) => {
	        	dbConn(this.tableName).select("*").where({product_id: data.product_id}).then((result) => {
					result = JSON.parse(JSON.stringify(result[0]))
					console.log(result);
					return callback(null, result);
				}).catch((error) => {
					console.log("Error occured while getting data", error)
					return callback({msg:"Something went wrong, we are working on it!"});
				});
	        }]
	    }, (error,asyncAutoResult) => {
	    	if(error) {
	    		console.log(error);
	    		return cb(null, customCatchError(error));
	    	} else {
				return cb(null, {success: true, data:asyncAutoResult.getProduct});
	    	}
	    });
	}

	createProduct(data, cb) {
		console.log("in here", data)
		async.auto({
	        validate: (callback) => {
	            var validate = true;

	            if (!validate) {
	                return callback("Please provide correct data");
	            }
	            // data = validatorFunctions.trimAll(data);
	            return callback(null, data);
	        },
	        createProduct : ['validate', (results, callback) => {
	        	dbConn(this.tableName).insert(data).then((result) => {
					console.log(result);
					result = JSON.parse(JSON.stringify(result[0]))
					return callback(null, result);
				}).catch((error) => {
					console.log("Error occured while getting data", error)
					return callback({msg:"Something went wrong, we are working on it!"});
				});
	        }],
	        getCreatedProduct: ['createProduct', (results, callback) => {
	        	this.getProduct({product_id: results.createProduct}, (error, result) => {
	        		if(error) {
			    		console.log(error);
			    		return callback(error);
			    	} else {
						return callback(null, result.data);
			    	}
	        	})
	        }]
	    }, (error,asyncAutoResult) => {
	    	if(error) {
	    		console.log(error);
	    		return cb(null, customCatchError(error));
	    	} else {
	    		// _.forEach(asyncAutoResult.createProduct, (value, key) => {
				// 	console.log("value", value);
				// });
				return cb(null, {success: true, message: "Product Created Sucessfully!", data: asyncAutoResult.getCreatedProduct});
	    	}
	    });
	}

	updateProduct(data, cb) {
		console.log("in here", data)
		async.auto({
	        validate: (callback) => {
	            var validate = true;

	            if (!validate) {
	                return callback("Please provide correct data");
	            }
	            // data = validatorFunctions.trimAll(data);
	            return callback(null, data);
	        },
	        updateProduct : ['validate', (results, callback) => {
	        	dbConn(this.tableName).update(data).where({product_id:data.product_id}).returning("*").then((result) => {
					console.log("result", result);
					return callback(null, result);
				}).catch((error) => {
					console.log("Error occured while getting data", error)
					return callback({msg:"Something went wrong, we are working on it!"});
				});
	        }],
	        getUpdatedProduct: ['updateProduct', (results, callback) => {
	        	this.getProduct({product_id: data.product_id}, (error, result) => {
	        		if(error) {
			    		console.log(error);
			    		return callback(error);
			    	} else {
						return callback(null, result.data);
			    	}
	        	})
	        }]
	    }, (error,asyncAutoResult) => {
	    	if(error) {
	    		console.log(error);
	    		return cb(null, customCatchError(error));
	    	} else {
				return cb(null, {success: true, message: "Product Updated Sucessfully!" , data: asyncAutoResult.getUpdatedProduct});
	    	}
	    });
	}

	// getMsgs();
}

module.exports = Product;
