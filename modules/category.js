'use strict'
var serverHelper = require('./../server/utility/server-helper');
var dbConn = require('./../server/db-connect')()["mariadb"];
var async = serverHelper.async;
var customCatchError = serverHelper.customCatchError;
var _ = serverHelper._;

class Category {
	constructor() {
		this.table = "category";
	}

	getCategories = (data, cb) => {
		console.log("data ==>", data)
		async.auto({
			validate: (callback) => {
				//Write validations here
				return callback(null, {})
			},
			getCategories: ['validate', (results, callback) => {
				dbConn.raw("Select * from category").then((result) => {
					result = JSON.parse(JSON.stringify(result[0]))
					return callback(null, result);
				}).catch((error) => {
					console.log("Error occured", error)
					return callback({message: "Error occured while getting categories, try again later !"})
				})
			}]
		}, (error, asyncAutoResult) => {
			if(error) {
				console.log("Error occured", error)
				return cb(null, customCatchError(error));
			} else {
				return cb(null, {success: true, data: asyncAutoResult.getCategories});
			}
		})
	}

	getCategory = (data, cb) => {
		console.log("data ==>", data)
		async.auto({
			validate: (callback) => {
				//Write validations here
				return callback(null, {})
			},
			getCategory: ['validate', (results, callback) => {
				dbConn(this.table).select("*").where({category_id: data.category_id}).then((result) => {
					result = JSON.parse(JSON.stringify(result))
					return callback(null, result[0]);
				}).catch((error) => {
					console.log("Error occured", error)
					return callback({message: "Error occured while getting categories, try again later !"})
				})
			}]
		}, (error, asyncAutoResult) => {
			if(error) {
				console.log("Error occured", error)
				return cb(null, customCatchError(error));
			} else {
				return cb(null, {success: true, data: asyncAutoResult.getCategory});
			}
		})
	}

	createCategory = (data, cb) => {
		console.log("data ==>", data)
		async.auto({
			validate: (callback) => {
				//Write validations here
				return callback(null, {})
			},
			createCategory: ['validate', (results, callback) => {
				dbConn(this.table).insert(data).then((result) => {
					result = JSON.parse(JSON.stringify(result[0]))
					return callback(null, result);
				}).catch((error) => {
					console.log("Error occured", error)
					return callback({message: "Error occured while getting categories, try again later !"})
				})
			}],
			getCreatedCategry: ['createCategory', (results, callback) => {
				this.getCategory({category_id: results.createCategory}, (error, result) => {
					if(error) {
						console.log("Error occured", error)
						return callback(error);
					} else {
						return callback(null, result.data);
					}
				})
			}]
		}, (error, asyncAutoResult) => {
			if(error) {
				console.log("Error occured", error)
				return cb(null, customCatchError(error));
			} else {
				return cb(null, {success: true, message: "Category created successfully", data: asyncAutoResult.getCreatedCategry});
			}
		})
	}

	updateCategory = (data, cb) => {
		console.log("data ==>", data)
		async.auto({
			validate: (callback) => {
				//Write validations here
				return callback(null, {})
			},
			updateCategory: ['validate', (results, callback) => {
				dbConn(this.table).update(data).where({category_id: data.category_id}).then((result) => {
					return callback(null, result);
				}).catch((error) => {
					console.log("Error occured", error)
					return callback({message: "Error occured while getting categories, try again later !"})
				})
			}],
			getUpdatedCategry: ['updateCategory', (results, callback) => {
				this.getCategory({category_id: data.category_id}, (error, result) => {
					if(error) {
						console.log("Error occured", error)
						return callback(error);
					} else {
						return callback(null, result.data);
					}
				})
			}]
		}, (error, asyncAutoResult) => {
			if(error) {
				console.log("Error occured", error)
				return cb(null, customCatchError(error));
			} else {
				return cb(null, {success: true, message: "Category updated successfully", data: asyncAutoResult.getUpdatedCategry});
			}
		})
	}
}

module.exports = Category