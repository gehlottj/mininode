const request = require('supertest');
const app = require('../server/server');
var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;

describe('Category API', () => {
  let server;

  before(() => {
    it('###### Starting Category unit tests #######', async () => {
      // server = app.listen(4000);
    });
  });

  after(() => {
    it('###### Unit Category tests completed #######', async () => {
    });
  });

  it('should return all Categories', async () => {
  		const res = await request(app).get('/api/category/getCategories');
  		expect(res.status).to.equal(200);
  		expect(res.body.success).to.equal(true);
  		expect(res.body.data).to.be.an("array");
  });

  it('should return category for an id', async () => {
  		reqBody = {
  			category_id: 1
  		}
  		const res = await request(app).post('/api/category/getCategory').send(reqBody);
  		expect(res.status).to.equal(200);
  		expect(res.body.success).to.equal(true);
  		expect(res.body.data).to.be.an("object");
  		expect(res.body.data.category_id).to.equal(reqBody.category_id);
  });

  it('should return create a new category', async () => {
  		reqBody = {
            "name": "Devices",
            "description": "Mobile Phones"
        }
  		const res = await request(app).post('/api/category/createCategory').send(reqBody);
  		expect(res.status).to.equal(200);
  		expect(res.body.success).to.equal(true);
  		expect(res.body.data).to.be.an("object");
  		expect(res.body.data.name).to.equal(reqBody.name);
  });

  it('should return update an existing category', async () => {
  		reqBody = {
  			"category_id": 4,
            "name": "Devices Testing",
            "description": "Mobile Phones Testing"
        }
  		const res = await request(app).post('/api/category/updateCategory').send(reqBody);
  		expect(res.status).to.equal(200);
  		expect(res.body.success).to.equal(true);
  		expect(res.body.data).to.be.an("object");
  		expect(res.body.data.category_id).to.equal(reqBody.category_id);
  		expect(res.body.data.name).to.equal(reqBody.name);
  });

});