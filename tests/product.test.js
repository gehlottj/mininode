const request = require('supertest');
const app = require('../server/server');
var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;

describe('Product API', () => {
  let server;

  before(() => {
    it('###### Starting product unit tests #######', async () => {
      // server = app.listen(4000);
    });
  });

  after(() => {
    it('###### Unit category tests completed #######', async () => {
    });
  });

  it('should return all products', async () => {
    const res = await request(app).get('/api/product/getProducts');
      expect(res.status).to.equal(200);
      expect(res.body.success).to.equal(true);
      expect(res.body.data).to.be.an('array');
  });

  it('should return product with product id', async () => {
    const reqBody = {
      product_id: 2
    }
    const res = await request(app).post('/api/product/getProduct').send(reqBody);
      expect(res.status).to.equal(200);
      expect(res.body.success).to.equal(true);
      expect(res.body.data).to.be.an('object');
      expect(res.body.data.product_id).to.equal(reqBody.product_id);
  });

  it('should create a new product', async () => {
    const reqBody = {
      "name": "Mac Laptop Demo Test",
      "description": "15.4\" Laptop with Intel Core i5 Processor",
      "price": 89.99,
      "category_id": 1
    }
    const res = await request(app).post('/api/product/createProduct').send(reqBody);
      expect(res.status).to.equal(200);
      expect(res.body.success).to.equal(true);
      expect(res.body.data).to.be.an('object');
      expect(res.body.data.name).to.equal(reqBody.name);
      expect(res.body.data.price).to.equal(reqBody.price);
      expect(res.body.data.category_id).to.equal(reqBody.category_id);
  });

  it('should update an existing product', async () => {
    const reqBody = {
      "product_id" : 14,
      "name": "Mac Laptop Demo Test Updated",
      "description": "15.4\" Laptop with Intel Core i5 Processor",
      "price": 849.99,
      "category_id": 1
    }
    const res = await request(app).post('/api/product/updateProduct').send(reqBody);
      expect(res.status).to.equal(200);
      expect(res.body.success).to.equal(true);
      expect(res.body.data).to.be.an('object');
      expect(res.body.data.product_id).to.equal(reqBody.product_id);
      expect(res.body.data.name).to.equal(reqBody.name);
      expect(res.body.data.price).to.equal(reqBody.price);
      expect(res.body.data.category_id).to.equal(reqBody.category_id);
  });

  // Add more tests for other routes (GET by id, PUT, DELETE)
});
